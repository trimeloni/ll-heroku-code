'use strict';

/* Controllers */
function ContactListCtrl($scope, $location, ContactList ) {
    $scope.goodSubmission = false;

    $scope.SaveContact = function(){
        
        $scope.goodSubmission = false;

        // Save the new recipe
        var newContact = new ContactList();
        newContact.name = $scope.name;
        newContact.email = $scope.email;
        newContact.phone = $scope.phone;
        newContact.eventDate = $scope.eventDate;
        newContact.eventType = $scope.eventType;
        newContact.eventLength = $scope.eventLength;
        newContact.comment = $scope.comment;

        newContact.$save(function(item, putResponseHeaders) {
            $scope.name = "";
            $scope.email = "";
            $scope.phone = "";
            $scope.eventDate = "";
            $scope.eventType = "";
            $scope.eventLength = "";
            $scope.comment = "";

            $scope.goodSubmission = true;
        });
    };

}
