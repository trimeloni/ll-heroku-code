'use strict';

/* Services */
angular.module('myAppServices', ['ngResource'])
        .factory('ContactList', function($resource){
           return $resource('/api/contact');
        });
        

  
