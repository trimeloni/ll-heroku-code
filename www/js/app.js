'use strict';

// Declare app level module which depends on filters, and services
angular.module('myApp', ['myAppServices','myRecipeApp.directives']).
  config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/home', {templateUrl: 'partials/Home.html', controller: ''});
    $routeProvider.when('/signup', {templateUrl: 'partials/SignUp.html', controller: 'ContactListCtrl'});
    $routeProvider.when('/aboutus', {templateUrl: 'partials/AboutUs.html', controller: ''});
    $routeProvider.when('/contactus', {templateUrl: 'partials/ContactUs.html', controller: ''});
    $routeProvider.when('/information', {templateUrl: 'partials/Information.html', controller: ''});
    $routeProvider.when('/packages', {templateUrl: 'partials/Packages.html', controller: ''});

    $routeProvider.otherwise({redirectTo: '/home'});
  }]);

