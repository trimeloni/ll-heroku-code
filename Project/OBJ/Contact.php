<?php

namespace OBJ;

/**
 * @Entity @Table(name="contact")
 **/
class Contact
{
    /** @Id @Column(name="contactId",type="integer") @GeneratedValue **/
    public $contactId;

    /** @Column(type="string") **/
    public $name;

    /** @Column(type="string") **/
    public $email;

    /** @Column(type="string") **/
    public $phone;

    /** @Column(type="string") **/
    public $eventDate;

    /** @Column(type="string") **/
    public $eventType;

    /** @Column(type="string") **/
    public $eventLength;

    /** @Column(type="string") **/
    public $comment;



    public function JSonEncode()
    {
        return json_encode($this);
    }

    public function ToReadableString()
    {
	return "Name: " . $this->name . "\n" . "Email: " . $this->email . "\n"
             . "Phone: " . $this->phone . "\n"
             . "Event Date: " . $this->eventDate . "\n"
             . "Event Type: " . $this->eventType . "\n"
             . "Event Length: " . $this->eventLength . "\n"
             . "Comment: " . $this->comment . "\n";
    }

}






