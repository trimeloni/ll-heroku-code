<?php

include_once("APIBase.php");


/**
 * Work with the collection of contacts.
 *   
 *
 * @author Atrimeloni
 * @uri /contact
 */
class Contacts extends APIBase  {  

//    /**
//     * @method GET
//     */
//    public function GetContacts()
//    {
//        $contactDAL = new DAL\Contact();
//        $contact = $contactDAL->GetContacts();
//
//        //return $contact;
//        //return json_encode($contact);
//        return new Tonic\Response(Tonic\Response::OK,$contact,array('content-type'=>'application/json'));
//    }
//
    /**
     * @method POST
     */
    public function AddContact()
    {    

	$json_data = json_decode($this->request->data);
	$mapper = new JsonMapper();
	$contactObject = $mapper->map($json_data, new OBJ\Contact());

	// Store into the Database        
        $contactDAL = new DAL\Contact();        
        $newContact = $contactDAL->CreateContact($contactObject);

	if (\CONF\Configuration::MAIL_SEND) {

		// Sending Mail to list of contacts
		//   For now, just sticking all of the fields here, but if we want to start sending email 
		//   from more than one place, then should centralize some of it (i.e. username/password/smtp/etc)
		$mail = new PHPMailer;

		//$mail->SMTPDebug = 3;                               // Enable verbose debug output (Higher number = more info)
		//$mail->SMTPDebug = 2;                               // Enable verbose debug output
		//$mail->SMTPDebug = 1;                               // Enable verbose debug output

		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = \CONF\Configuration::MAIL_HOST;  // Specify main and backup SMTP servers
		$mail->SMTPAuth = \CONF\Configuration::MAIL_SMTP_AUTH;                               // Enable SMTP authentication
		$mail->Username = \CONF\Configuration::MAIL_USER;                 // SMTP username - ADD THIS
		$mail->Password = \CONF\Configuration::MAIL_PASSWORD;				// SMTP Password - ADD THIS
		$mail->SMTPSecure = \CONF\Configuration::MAIL_SMTP_SECURE_METHOD;                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = \CONF\Configuration::MAIL_SMTP_PORT;                                    // TCP port to connect to
		$mail->setFrom(\CONF\Configuration::MAIL_FROM_ADDRESS, \CONF\Configuration::MAIL_FROM_NAME);
		$mail->addAddress(\CONF\Configuration::MAIL_TO_ADDRESS);               // Name is optional
		$mail->addReplyTo(\CONF\Configuration::MAIL_REPLY_TO_ADDRESS, \CONF\Configuration::MAIL_REPLY_TO_NAME);
		$mail->isHTML(false);                                  // Set email format to HTML
		
		$mail->Subject = \CONF\Configuration::MAIL_SUBJET;
		$mail->Body    = "Request Information Below: \n\n" . $contactObject->ToReadableString();

		if(!$mail->send()) {
		    echo 'Message could not be sent.';
		    echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
		    echo 'Message has been sent';
		}
	}
        return new Tonic\Response(Tonic\Response::CREATED,$newContact,array('content-type'=>'application/json'));
    }
    
//    /**
//     * @method PUT
//     * @json
//     */
//    public function UpdateContact()
//    {
//	$json_data = json_decode($this->request->data);
//        $mapper = new JsonMapper();
//        $contactObject = $mapper->map($json_data, new OBJ\Contact());
//
//
//        $contactDAL = new DAL\Contact();
//        $updatedContact = $contactDAL->UpdateContact($contactObject);
//        
//        return new Tonic\Response(Tonic\Response::ACCEPTED,$updatedContact,array('content-type'=>'application/json'));
//        
//    }
}



/**
 * Work with an individual Contact
 *
 * @author Atrimeloni
 * @uri /contact/{contactId}
 */
class Contact extends APIBase  {  
//    /**
//     * @param integer $contactId
//     * @return \Tonic\Response
//     * @method GET
//     */
//    public function GetContact($contactId)
//    {
//        $contactDAL = new DAL\Contact();
//        $contact = $contactDAL->GetContactByContactId($contactId);
//        
//        //return $contact;
//        return new Tonic\Response(Tonic\Response::OK,$contact,array('content-type'=>'application/json'));
//    }
//    
//    /**
//     * @param integer $contactId
//     * @return \Tonic\Response
//     * @method DELETE
//     */
//    public function DeleteContact($contactId)
//    {
//        $contactDAL = new DAL\Contact();
//        $contact = $contactDAL->DeleteContactByContactId($contactId);
//        
//        //return $contact;
//        return new Tonic\Response(Tonic\Response::OK,"OK");
//    }
//    
    
}




