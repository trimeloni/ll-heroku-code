<?php
// DAL

namespace DAL;

// BootStrap to Doctrine
require_once "DALBase.php";

/**
 * Description of Contact
 *
 * @author Atrimeloni
 */
class Contact extends DALBase
{
    public function GetContacts()
    {
        $repository = $this->entityManager->getRepository('OBJ\Contact');
        $objects = $repository->findAll();

        return $objects;
    }
    
    public function GetContactByContactId($contactId)
    {
        $object = $this->entityManager->find('OBJ\Contact', $contactId); // by PK
        return $object;
    }

    public function UpdateContact($contact)
    { 
        // get the entry from the database (by id) and merge it to the new values
        $updatedObject = $this->entityManager->merge($contact);

        // perform the update to the database
        $this->entityManager->flush();
        
        return $updatedObject;
    }
    
    public function CreateContact($contact)
    {
        $this->entityManager->persist($contact);
        $this->entityManager->flush();
        
        return $contact;
    }
    
    public function DeleteContactByContactId($ContactId)
    {
        $object = $this->entityManager->find('OBJ\Contact', $contactId); // by PK
        $this->entityManager->remove($object);
        $this->entityManager->flush();
        
        return;
    }
    
    
    
}
    
